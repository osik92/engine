﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameEngine.Utilities
{
    public static class ObjectExtensions
    {
        private static readonly ConditionalWeakTable<object, RefId> _ids = new ConditionalWeakTable<object, RefId>();

        public static Guid GetRefId<T>(this T obj) where T : class
        {
            if(obj == null)
                return default(Guid);

            return _ids.GetOrCreateValue(obj).Id;
        }

        private class RefId
        {
            public Guid Id { get; } = Guid.NewGuid();
        }

        public static T Clone<T>(this T obj)
        {
            using(MemoryStream ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);

                ms.Seek(0, SeekOrigin.Begin);
                T clone = (T)formatter.Deserialize(ms);
                return clone;
            }
        }
    }
}