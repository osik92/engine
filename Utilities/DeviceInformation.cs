﻿using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine.Utilities
{
    public class DeviceInformation
    {
        private static string Information(string stringIn, List<string> propsName)
        {
            StringBuilder builder = new StringBuilder();

            ManagementClass ManagementClass1 = new ManagementClass(stringIn);
            //Create a ManagementObjectCollection to loop through
            ManagementObjectCollection ManagemenobjCol = ManagementClass1.GetInstances();
            //Get the properties in the class
            PropertyDataCollection properties = ManagementClass1.Properties;
            foreach (ManagementObject obj in ManagemenobjCol)
            {
                foreach (PropertyData property in properties)
                {
                    try
                    {
                        if (propsName.Contains(property.Name))
                        {
                            builder.AppendLine(property.Name + ":  " +
                                               obj.Properties[property.Name].Value.ToString());
                        }
                    }
                    catch
                    {
                        //Add codes to manage more informations
                    }
                }
            }


            return builder.ToString();
        }

        public static string GPUInormation()
        {
            List<string> propertyNames = new List<string>();

            propertyNames.Add("Caption");
            propertyNames.Add("DriverVersion");
            propertyNames.Add("VideoModeDescription");
            propertyNames.Add("DeviceID");

            string key = "Win32_VideoController";
            return Information(key, propertyNames);
        }

        public static string CPUInormation()
        {
            List<string> propertyNames = new List<string>();

            propertyNames.Add("AddressWidth");
            propertyNames.Add("Name");
            propertyNames.Add("NumberOfCores");
            propertyNames.Add("NumberOfLogicalProcessors");

            string key = "Win32_Processor";
            return Information(key, propertyNames);
        }

        public static string OSInformation()
        {
            List<string> propertyNames = new List<string>();

            propertyNames.Add("Caption");
            propertyNames.Add("BuildNumber");
            propertyNames.Add("OSArchitecture");
            propertyNames.Add("TotalVirtualMemorySize");
            propertyNames.Add("TotalVisibleMemorySize");
            propertyNames.Add("FreePhysicalMemory");
            propertyNames.Add("FreeVirtualMemory");
            string key = "Win32_OperatingSystem";
            return Information(key, propertyNames);

        }

    }
}
