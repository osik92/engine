using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace GameEngine.Logger
{
    public class JsonLogger
    {
        internal class LogInfo
        {
            public LogInfoTime Time { get; set; }
            public string File { get; set; }
            public int Line { get; set; }
            public string Function { get; set; }
            public string Type { get; set; }
            public string Message { get; set; }
        }

        internal class LogInfoTime
        {
            public int Hour { get; set; }
            public int Minute { get; set; }
            public int Second { get; set; }
            public int Millisecond { get; set; }
        }

        private List<LogInfo> logs;
        public JsonLogger()
        {
            logs = new List<LogInfo>();
        }

        public void Log(DateTime time, string filepath, int lineNumber, string functionName, Logger.LogType type,
            string message)
        {
            DirectoryInfo di = new DirectoryInfo(filepath);
            string file = di.Parent + "\\" + di.Name;

            LogInfo li = new LogInfo
            {
                Time = new LogInfoTime()
                {
                    Hour = time.Hour,
                    Minute = time.Minute,
                    Second = time.Second,
                    Millisecond = time.Millisecond

                },
                File = file,
                Function = functionName,
                Line = lineNumber,
                Message = message,
                Type = type.ToString()

            };
            logs.Add(li);
        }

        public void SaveToFile(string filepath)
        {
            using (StreamWriter writer = new StreamWriter(filepath))
            {
                writer.WriteLine(JsonConvert.SerializeObject(logs));
            }
        }
    }
}