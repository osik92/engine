﻿#define LOGGER_ENABLE
using System;
using System.Runtime.CompilerServices;

namespace GameEngine.Logger
{
    public class Logger
    {
        public enum LogType
        {
            Message,
            Warning,
            Error
        }

        private static Action<DateTime, string, int, string, LogType, string> loggerOutput;

        public static void LogError(string message, [CallerMemberName] string method = "",
            [CallerFilePath] string filepath = "", [CallerLineNumber] int lineNumber = 0)
        {
            Log(DateTime.Now, filepath, lineNumber, method, LogType.Error, message);
        }

        public static void LogWarning(string message, [CallerMemberName] string method = "",
            [CallerFilePath] string filepath = "", [CallerLineNumber] int lineNumber = 0)
        {
            Log(DateTime.Now, filepath, lineNumber, method, LogType.Warning, message);
        }

        public static void LogMessage(string message, [CallerMemberName] string method = "",
            [CallerFilePath] string filepath = "", [CallerLineNumber] int lineNumber = 0)
        {
            Log(DateTime.Now, filepath, lineNumber, method, LogType.Message, message);
        }

        private static void Log(DateTime dateTime, string file, int line, string functionName, LogType type,
            string message)
        {
            #if LOGGER_ENABLE
                loggerOutput?.Invoke(dateTime, file, line, functionName, type, message);
            #endif
        }

        public static void RegisterOutput(Action<DateTime, string, int, string, LogType, string> output)
        {
            loggerOutput += output;
        }

        public static void UnregisterOutput(Action<DateTime, string, int, string, LogType, string> output)
        {
            loggerOutput -= output;
        }
    }
}
