using System;

namespace GameEngine.Logger
{
    public class ConsoleLogger
    {
        public static void Log(DateTime time, string filepath, int lineNumber, string functionName, Logger.LogType type,
            string message)
        {
            Console.Write($"[{time:HH:mm:ss.fff}][{filepath}][{functionName}][{lineNumber}]");
            switch (type)
            {
                case Logger.LogType.Error:
                    LogError(message);
                    break;
                case Logger.LogType.Warning:
                    LogWarning(message);
                    break;
                default:
                    LogMessage(message);
                    break;
            }

        }

        private static void LogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"[ERR] : {message}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void LogMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"[MSG] : {message}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void LogWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"[WRN] : {message}");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}