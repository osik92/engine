﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RectangleExtension = GameEngine.Utilities.RectangleExtension;

namespace GameEngine.GUI
{
    public class ListItem : Control
    {
        private string text;
        private Color backColor;
        private float scale;
        private bool isHover;

        public ListItem(string text)
        {
            this.text = text;
            IsActive = true;
        }

        public override void Update(GameTime gameTime)
        {
            if(IsSelected)
            {
                scale = 1.15f;
                backColor = SelectedBackColor;
            }

            else
            {
                backColor = BackColor;
                scale = isHover ? (float)(1.0f + 0.15 * Math.Abs(Math.Sin((double)gameTime.TotalGameTime.TotalSeconds * 2))) : 1.0f;
            }



            isHover = false;
            IsSelected = false;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {
            Vector2 textSize = TextFont.MeasureString(text);

            Vector2 textCenter = new Vector2(textSize.X / 2.0f, textSize.Y / 2.0f);
            Vector2 textPos = new Vector2(Size.X / 2.0f + Bounds.X, Size.Y / 2.0f + Bounds.Y);


            spriteBatch.Draw(RectangleExtension.SolidTexture(spriteBatch), Bounds, backColor * alpha);

            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y), new Vector2(Bounds.X + Bounds.Width, Bounds.Y), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y + Bounds.Height), new Vector2(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y), new Vector2(Bounds.X, Bounds.Y + Bounds.Height), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X + Bounds.Width, Bounds.Y), new Vector2(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height), Color.Black);

            spriteBatch.DrawString(TextFont, text, textPos, FrontColor * alpha, 0, textCenter, scale, SpriteEffects.None, 0);

        }

        public override void OnHover(Point position)
        {
            if(IsActive)
                isHover = true;
        }

        public void OnClick()
        {
            if(IsActive)
            {
                Logger.Logger.LogMessage($"{ToString()} clicked");
                ClickAction(this);
            }

        }

        internal Vector2 Position
        {
            get
            {
                return new Vector2(Bounds.X, Bounds.Y);
            }

            set
            {
                Point pos = value.ToPoint();

                Bounds = new Rectangle(pos.X, pos.Y, (int)Size.X, (int)Size.Y);
            }
        }

        internal Vector2 Size
        {
            get
            {
                return Bounds.Size.ToVector2();
            }

            set
            {
                Point size = value.ToPoint();
                Bounds = new Rectangle((int)Position.X, (int)Position.Y, size.X, size.Y);
            }
        }

        public Color FrontColor;
        public Color BackColor;
        public Color SelectedBackColor;

        public delegate void Click(ListItem sender);

        public Click ClickAction;

        internal bool IsSelected;

        public String Text
        {
            get
            {
                return text;
            }
            set
            {
                if(value != String.Empty)
                {
                    text = value;
                }
            }
        }

        public override string ToString()
        {
            return $"{this.GetType().FullName} {Text}";
        }
    }
}