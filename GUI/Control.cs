﻿using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using EngineMath = GameEngine.Utilities.Math;

namespace GameEngine.GUI
{
    public abstract class Control
    {
        public Color HoverColor;
        public SpriteFont TextFont;
        internal GUI gui;

        public bool IsActive { get; set; }

        public Rectangle Bounds { get; protected set; }

        public abstract void Update(GameTime gameTime);

        public abstract void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha);

        public abstract void OnHover(Point position);

        public virtual void Activate() { }


    }
}