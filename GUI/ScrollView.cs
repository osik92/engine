﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameEngine.GUI
{
    public class ScrollView : Control
    {
        private GUI scrollGui;


        public ScrollView(InputHandler handler)
        {
            scrollGui = new GUI(handler);
        }
        public override void Update(GameTime gameTime)
        {
            scrollGui.Update(gameTime);

        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {

            RenderTarget2D target = new RenderTarget2D(spriteBatch.GraphicsDevice, Bounds.Width, Bounds.Height, false, SurfaceFormat.Bgr32SRgb, DepthFormat.None);

            //SpriteBatch sb = new SpriteBatch(spriteBatch.GraphicsDevice);
            
            spriteBatch.GraphicsDevice.SetRenderTarget(target);
            //spriteBatch.GraphicsDevice.Clear(Color.Red);

            //sb.Begin();
            //scrollGui.Draw(sb, gameTime, alpha);
            //sb.End();


            spriteBatch.GraphicsDevice.SetRenderTarget(null);

            //spriteBatch.Draw(target, Bounds, Color.White);

        }

        public Rectangle Bounds
        {
            get { return base.Bounds; }
            set { base.Bounds = value; }
        }

        public override void OnHover(Point position)
        {
            //throw new System.NotImplementedException();
        }

        public void Add(Control control)
        {
            scrollGui.AddControl(control);
        }

    }
}