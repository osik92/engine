﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GameEngine.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameEngine.Input;

namespace GameEngine.GUI
{

    public class List : Control
    {
        public Rectangle Bounds
        {
            get
            {
                return base.Bounds;
            }
            set
            {
                base.Bounds = value;
            }
        }

        private List<ListItem> elementList = new List<ListItem>();
        private SpriteBatch localSpriteBatch;
        private int displayedItems;
        private int selectedItem = -1;

        public override void Update(GameTime gameTime)
        {
            for(int i = 0; i < elementList.Count; i++)
            {
                var listItem = elementList[i];
                if(selectedItem == i)
                    listItem.IsSelected = true;
                listItem.Update(gameTime);
            }
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {
            if(localSpriteBatch == null)
                localSpriteBatch = new SpriteBatch(spriteBatch.GraphicsDevice);

            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y),
                new Vector2(Bounds.X + Bounds.Width, Bounds.Y), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y + Bounds.Height),
                new Vector2(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y),
                new Vector2(Bounds.X, Bounds.Y + Bounds.Height), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X + Bounds.Width, Bounds.Y),
                new Vector2(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height), Color.Black);

            Vector2 itemSize = new Vector2(Bounds.Width, Bounds.Height / displayedItems);

            for(int i = 0; i < elementList.Count; i++)
            {
                elementList[i].TextFont = TextFont;
                elementList[i].Position = new Vector2(Bounds.X, Bounds.Y + itemSize.Y * i);
                elementList[i].Size = itemSize;
                elementList[i].Draw(spriteBatch, gameTime, alpha * 0.75f);
            }
        }

        public override void OnHover(Point position)
        {
            foreach(ListItem item in elementList)
            {
                if(item.Bounds.Contains(position))
                {
                    item.OnHover(position);
                    if(gui.inputHandler.IsMouseButtonDown(eMouseButton.Left))
                        item.OnClick();
                }
            }
        }

        public void Add(ListItem item)
        {
            Logger.Logger.LogMessage($"Add {item.ToString()} to {this.ToString()}");
            item.ClickAction += ClickAction;
            item.gui = gui;
            elementList.Add(item);
        }

        public bool RemoveAt(int index)
        {
            if(index == -1)
                return false;
            try
            {
                elementList.RemoveAt(index);
                selectedItem = -1;
                return true;
            }
            catch(Exception)
            {
                Logger.Logger.LogError($"Cannot removed list ({this.GetRefId()}) item index {index}");
                return false;
            }
        }

        private void ClickAction(ListItem sender)
        {
            if(elementList.Contains(sender))
            {
                selectedItem = elementList.IndexOf(sender);
                Logger.Logger.LogMessage($"List selected index: {selectedItem}");
            }
        }

        public int Count
        {
            get
            {
                return elementList.Count;
            }
        }

        public int SelectedItem
        {
            get
            {
                return selectedItem;
            }
        }

        public void SelectItem(int index)
        {
            selectedItem = index;
        }

        public List(int displayedItems)
        {
            this.displayedItems = displayedItems;
            IsActive = true;
        }
        public List<ListItem> Items
        {
            get
            {
                return elementList;
            }
        }

        public override string ToString()
        {
            return $"{this.GetType().FullName} {this.GetRefId()}";
        }
    }
}
