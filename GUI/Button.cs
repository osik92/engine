﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RectangleExtension = GameEngine.Utilities.RectangleExtension;

namespace GameEngine.GUI
{
    public class Button : Control
    {
        public Color BackColor { get { return bc; } set { bc = backColor = value; } }

        public Color FrontColor { get { return fc; } set { fc = frontColor = value; } }

        protected Color bc;
        protected Color fc;

        protected Color backColor;
        protected Color frontColor;
        protected Vector2 position;
        public string Text;
        protected bool isHover = false;
        protected float scale = 1.0f;


        public Rectangle Bounds
        {
            get { return base.Bounds; }
            set { base.Bounds = value; }
        }

        public delegate void Click();

        public Click ClickAction;

        public Button(string text)
        {
            this.Text = text;
            IsActive = true;
        }

        public Vector2 Position
        {
            get { return position; }
            set
            {
                this.position = value;

                var textSize = TextFont.MeasureString(Text);
                this.Bounds = new Rectangle(position.ToPoint(), new Point((int)textSize.X + 20, (int)textSize.Y + 10));
            }
        }

        public override void Update(GameTime gameTime)
        {
            frontColor = isHover ? HoverColor : FrontColor;
            scale = isHover ? (float)(1.0f + 0.15 * Math.Abs(Math.Sin((double)gameTime.TotalGameTime.TotalSeconds * 2))) : 1.0f;

            backColor = BackColor;

            if (!IsActive)
                frontColor = FrontColor * 0.5f;

            isHover = false;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {
            Vector2 textSize = TextFont.MeasureString(Text);

            Vector2 textCenter = new Vector2(textSize.X / 2.0f, textSize.Y / 2.0f);
            Vector2 textPos = new Vector2(Bounds.Width/  2.0f + Bounds.X, Bounds.Height / 2.0f + Bounds.Y);


            var rescaleRect = new Rectangle(Bounds.Location, new Point((int)(Bounds.Width * scale), (int)(Bounds.Height * scale)));

            spriteBatch.Draw(RectangleExtension.SolidTexture(spriteBatch), Bounds, backColor * alpha);

            if (IsActive)
            { 
                spriteBatch.DrawString(TextFont, Text, textPos + new Vector2( 1, 1), Color.Black * alpha, 0, textCenter, scale, SpriteEffects.None, 0);
                spriteBatch.DrawString(TextFont, Text, textPos + new Vector2( 1,-1), Color.Black * alpha, 0, textCenter, scale, SpriteEffects.None, 0);
                spriteBatch.DrawString(TextFont, Text, textPos + new Vector2(-1,-1), Color.Black * alpha, 0, textCenter, scale, SpriteEffects.None, 0);
                spriteBatch.DrawString(TextFont, Text, textPos + new Vector2(-1, 1), Color.Black * alpha, 0, textCenter, scale, SpriteEffects.None, 0);
            }
            spriteBatch.DrawString(TextFont, Text, textPos, frontColor * alpha, 0, textCenter, scale, SpriteEffects.None, 0);
        }

        public override void OnHover(Point position)
        {
            if (IsActive)
                isHover = true;
        }

        public void OnClick()
        {
            if (IsActive)
            {
                Logger.Logger.LogMessage($"Button {this.Text} Clicked");
                ClickAction();
            }
                
        }

        public override string ToString()
        {
            return $"{this.GetType().FullName} '{this.Text}'";
        }
    }
}