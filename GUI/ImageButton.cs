﻿using GameEngine.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Math = System.Math;

namespace GameEngine.GUI
{
    public class ImageButton : Button
    {
        private Image image;

        public ImageButton(Image image) : base(null)
        {
            this.image = image;
        }

        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
                this.Bounds = new Rectangle(position.ToPoint(), image.Size.ToPoint());
            }
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {
            var rescaleRect = new Rectangle(Bounds.Location, new Point((int)(Bounds.Width * scale), (int)(Bounds.Height * scale)));


            spriteBatch.DrawLine(rescaleRect.Location.ToVector2(), new Vector2(rescaleRect.Right, rescaleRect.Top), BackColor * alpha, 1);
            spriteBatch.DrawLine(rescaleRect.Location.ToVector2(), new Vector2(rescaleRect.Left, rescaleRect.Bottom), BackColor * alpha, 1);
            spriteBatch.DrawLine(new Vector2(rescaleRect.Right, rescaleRect.Top), (rescaleRect.Location + rescaleRect.Size).ToVector2(), BackColor * alpha, 1);
            spriteBatch.DrawLine(new Vector2(rescaleRect.Left, rescaleRect.Bottom), (rescaleRect.Location + rescaleRect.Size).ToVector2(), BackColor * alpha, 1);

            //spriteBatch.Draw(spriteBatch.SolidTexture(), rescaleRect, backColor * alpha);
            spriteBatch.Draw(image.Texture, new Rectangle(Position.ToPoint(), new Vector2(Bounds.Width * scale, Bounds.Height * scale).ToPoint()), image.SourceRectangle, frontColor * alpha);
            //spriteBatch.DrawString(TextFont, Text, Position + new Vector2(10, 5), frontColor * alpha, 0, Vector2.Zero, scale, SpriteEffects.None, 0);
        }

        public override void Update(GameTime gameTime)
        {
            backColor = isHover ? HoverColor : BackColor;
            scale = isHover ? (float)(1.0f + 0.15 * Math.Abs(Math.Sin((double)gameTime.TotalGameTime.TotalSeconds * 2))) : 1.0f;

            frontColor = FrontColor;
            isHover = false;
        }

        public override string ToString()
        {
            return $"{this.GetType().FullName} '{this.Text}'";
        }
    }
}