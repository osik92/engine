﻿using System.Collections.Generic;
using GameEngine.Input;
using GameEngine.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameEngine.GUI
{
    public class GUI
    {
        private List<Control> controls;
        internal InputHandler inputHandler;
        public SpriteFont TextFont;
        
        public GUI(InputHandler input)
        {
            controls = new List<Control>();
            this.inputHandler = input;
        }

        public void Update(GameTime gameTime)
        {
            foreach (Control control in controls)
            {
                if (control.IsActive)
                {
                    if (control.Bounds.Contains(inputHandler.GetMousePosition()))
                    {
                        control.OnHover(inputHandler.GetMousePosition());

                        if(control is Button && inputHandler.IsMouseButtonDown(eMouseButton.Left))
                            ((Button)control).OnClick();
                    }
                }
                control.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {
            foreach (Control control in controls)
            {
                control.Draw(spriteBatch, gameTime, alpha);
            }
        }

        public void AddControl(Control control)
        {
            Logger.Logger.LogMessage($"Add {control.ToString()} to {this.ToString()}");

            control.gui = this;
            control.Activate();
            controls.Add(control);
        }

        public override string ToString()
        {
            return $"{this.GetType().FullName} {this.GetRefId()}";
        }
    }
}
