﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameEngine.GUI
{
    public class Label : Control
    {
        private bool border = false;
        private string text;
        private Vector2 position;
        private Color frontColor;
        private Color borderColor;

        public Label(string text, bool border = false)
        {
            this.border = border;
            this.text = text;
            this.TextScale = 1.0f;
        }

        public override void Update(GameTime gameTime)
        {
            //throw new System.NotImplementedException();
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {
            var textSize = TextFont.MeasureString(text);

            if (border)
            {
                spriteBatch.DrawString(TextFont, text, position + new Vector2( 1,  0), borderColor, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                spriteBatch.DrawString(TextFont, text, position + new Vector2( 0,  1), borderColor, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                spriteBatch.DrawString(TextFont, text, position + new Vector2(-1,  0), borderColor, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                spriteBatch.DrawString(TextFont, text, position + new Vector2( 0, -1), borderColor, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
            }

            spriteBatch.DrawString(TextFont, text, position, frontColor, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
        }



        public override void OnHover(Point position)
        {
            //throw new System.NotImplementedException();
        }

        public String Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        public Rectangle Bounds
        {
            get
            {
                if (TextFont != null)
                {
                    return new Rectangle(this.Position.ToPoint(), TextFont.MeasureString(text).ToPoint());
                }
                else
                {
                    return new Rectangle(this.Position.ToPoint(), Vector2.Zero.ToPoint());
                }
            }
        }

        public float TextScale
        {
            get;
            set;
        }

        public Vector2 Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public Color FrontColor
        {
            get { return this.frontColor; }
            set { this.frontColor = value; }
        }

        public Color BorderColor
        {
            get { return this.borderColor; }
            set { this.borderColor = value; }
        }

        public bool Border
        {
            get { return this.border; }
            set { this.border = value; }
        }

        public override string ToString()
        {
            return $"{this.GetType().FullName} {this.Text}";
        }
    }
}