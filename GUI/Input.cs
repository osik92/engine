﻿using System;
using System.Runtime.CompilerServices;
using System.Text;
using GameEngine.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameEngine.GUI
{
    public class Input : Control
    {
        public Color FrontColor;
        public Color BackColor;
        private StringBuilder sb;

        private bool canWrite = false;
        public Input()
        {

            sb = new StringBuilder();
        }

        public override void Activate()
        {
            gui.inputHandler.KeyDown += KeyDown;
        }

        private void KeyDown(Keys key)
        {
            if (!IsActive)
                return;



            if (canWrite)
            {

                if (key.Equals(Keys.Back))
                {
                    if (sb.Length > 0)
                        sb.Remove(sb.Length - 1, 1);
                    return;
                }

                if (key.Equals(Keys.Space))
                {
                    sb.Append(" ");
                }

                char cKey = (char)key;

                if (cKey >= 65 && cKey <= 90) // litery 65 - 90
                {
                    if (gui.inputHandler.IsKeyPress(Keys.LeftShift) ||
                        gui.inputHandler.IsKeyPress(Keys.RightShift))
                    {
                        if (gui.inputHandler.IsKeyPress(Keys.LeftAlt) || gui.inputHandler.IsKeyPress(Keys.RightAlt))
                        {
                            switch (cKey)
                            {
                                case 'A':
                                    sb.Append("Ą");
                                    break;
                                case 'C':
                                    sb.Append("Ć");
                                    break;
                                case 'E':
                                    sb.Append("Ę");
                                    break;
                                case 'L':
                                    sb.Append("Ł");
                                    break;
                                case 'N':
                                    sb.Append("Ń");
                                    break;
                                case 'S':
                                    sb.Append("Ś");
                                    break;
                                case 'X':
                                    sb.Append("Ź");
                                    break;
                                case 'Z':
                                    sb.Append("Ż");
                                    break;
                                default:
                                    sb.AppendFormat($"{(char)(cKey)}");
                                    break;
                            }
                        }
                        else
                        {
                            sb.AppendFormat($"{(char)(cKey)}");
                        }
                    }
                    else
                    {
                        if (gui.inputHandler.IsKeyPress(Keys.LeftAlt) || gui.inputHandler.IsKeyPress(Keys.RightAlt))
                        {
                            switch (cKey)
                            {
                                case 'A':
                                    sb.Append("ą");
                                    break;
                                case 'C':
                                    sb.Append("ć");
                                    break;
                                case 'E':
                                    sb.Append("ę");
                                    break;
                                case 'L':
                                    sb.Append("ł");
                                    break;
                                case 'N':
                                    sb.Append("ń");
                                    break;
                                case 'S':
                                    sb.Append("ś");
                                    break;
                                case 'X':
                                    sb.Append("ź");
                                    break;
                                case 'Z':
                                    sb.Append("ż");
                                    break;
                                default:
                                    sb.AppendFormat($"{(char)(cKey + 32)}");
                                    break;
                            }
                        }
                        else
                        {
                            sb.AppendFormat($"{(char)(cKey + 32)}");
                        }
                    }

                }
                else if (cKey >= 48 && cKey <= 57) // cyfry 48 - 57
                {
                    if (gui.inputHandler.IsKeyPress(Keys.LeftShift) ||
                        gui.inputHandler.IsKeyPress(Keys.RightShift))
                    {
                        switch (cKey)
                        {
                            case '1':
                                sb.Append("!");
                                break;
                            case '2':
                                sb.Append("@");
                                break;
                            case '3':
                                sb.Append("#");
                                break;
                            case '4':
                                sb.Append("$");
                                break;
                            case '5':
                                sb.Append("%");
                                break;
                            case '6':
                                sb.Append("^");
                                break;
                            case '7':
                                sb.Append("&");
                                break;
                            case '8':
                                sb.Append("*");
                                break;
                            case '9':
                                sb.Append("(");
                                break;
                            case '0':
                                sb.Append(")");
                                break;
                        }
                    }
                    else
                    {
                        sb.AppendFormat($"{(char)(cKey)}");
                    }
                }
                else if (cKey >= 96 && cKey <= 105) // cyfry numpad 96 - 105
                {
                    sb.AppendFormat($"{(char)(cKey - 48)}");
                }
                else if (key.Equals(Keys.OemMinus))
                {
                    if (gui.inputHandler.IsKeyPress(Keys.LeftShift) ||
                        gui.inputHandler.IsKeyPress(Keys.RightShift))
                    {
                        sb.Append("_");
                    }
                    else
                    {
                        sb.Append("-");
                    }
                }
                else if (key.Equals(Keys.OemPlus))
                {
                    if (gui.inputHandler.IsKeyPress(Keys.LeftShift) ||
                        gui.inputHandler.IsKeyPress(Keys.RightShift))
                    {
                        sb.Append("+");
                    }
                    else
                    {
                        sb.Append("=");
                    }
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            return;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, float alpha)
        {
            Vector2 textSize = TextFont.MeasureString(Text);

            Vector2 textCenter = new Vector2(textSize.X / 2.0f, textSize.Y / 2.0f);
            Vector2 textPos = new Vector2(Bounds.Width / 2.0f + Bounds.X, Bounds.Height / 2.0f + Bounds.Y);


            spriteBatch.Draw(RectangleExtension.SolidTexture(spriteBatch), Bounds, BackColor * alpha);

            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y), new Vector2(Bounds.X + Bounds.Width, Bounds.Y), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y + Bounds.Height), new Vector2(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X, Bounds.Y), new Vector2(Bounds.X, Bounds.Y + Bounds.Height), Color.Black);
            RectangleExtension.DrawLine(spriteBatch, new Vector2(Bounds.X + Bounds.Width, Bounds.Y), new Vector2(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height), Color.Black);

            spriteBatch.DrawString(TextFont, Text, textPos, FrontColor * alpha, 0, textCenter, Vector2.One, SpriteEffects.None, 0);
        }

        public override void OnHover(Point position)
        {
            //canWrite = true;
        }

        public string Text
        {
            get { return sb.ToString(); }
            set { sb = new StringBuilder(value); }
        }

        public Rectangle Bounds
        {
            get { return base.Bounds; }
            set { base.Bounds = value; }
        }

        public bool CanWrite
        {
            get { return canWrite; }
            set { canWrite = value; }
        }

        public override string ToString()
        {
            return $"{this.GetType().FullName} {(CanWrite ? "can write" : "can't write")}";
        }
    }
}